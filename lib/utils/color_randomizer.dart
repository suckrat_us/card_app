import 'dart:math';


import 'package:flutter/material.dart';

Color colorRandomizer() {

  var rng = Random();
  int num = rng.nextInt(256);

  return Color.fromRGBO(num, rng.nextInt(256), rng.nextInt(256), 1.0);
}