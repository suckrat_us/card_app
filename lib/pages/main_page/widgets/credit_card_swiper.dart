import 'package:card_app/models/credit_card_dto.dart';
import 'package:card_app/pages/main_page/widgets/credit_card.dart';
import 'package:card_app/res/color.dart';
import 'package:flutter/material.dart';

class CreditCardSwiper extends StatelessWidget {
  final List<CreditCardDTO> cardList;

  CreditCardSwiper({@required this.cardList});

  List<Widget> buildCardList(List<CreditCardDTO> list) {

    List<Color> colorList = [PINK,AMBER, Colors.cyan,Colors.red,Colors.blue];
    List<Widget> _widgetList = List<Widget>();
    int i = 1;
    for(CreditCardDTO cardDTO in list) {

      _widgetList.add(
        CreditCardWidget(
          scale: 1.0 - (i/10),
          color: colorList[i-1],
        ),
      );
      i++;
    }

    return _widgetList;
  }
  @override
  Widget build(BuildContext context) {
    List<Widget> widgetList =  buildCardList(cardList).reversed.toList();
    return Container(
      width: double.infinity,
      height: 300,
      child: Stack(
        children: widgetList,
      ),
    );
  }
}
