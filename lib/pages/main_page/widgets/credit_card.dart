import 'package:flutter/material.dart';

class CreditCardWidget extends StatelessWidget {
  final Color color;
  final double scale;

  CreditCardWidget({@required this.color, this.scale = 1.0});
  @override


  Widget build(BuildContext context) {
    double _cardWidth   = 350.0 * scale;
    double _cardHeight  = (_cardWidth * .7) * scale;
    double margin = 55 * ((1.0 - scale) * 10);
//    print('Credit card ${scale*10} margin : $margin}');

    return Align(
      alignment: Alignment.center,
      child: Container(
        width: _cardWidth,
        height: _cardHeight,
        margin: EdgeInsets.only(top: margin),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          color: color,
        ),
      ),
    );
  }
}
