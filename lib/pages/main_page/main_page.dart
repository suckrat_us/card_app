import 'package:card_app/models/credit_card_dto.dart';
import 'package:card_app/models/information_dto.dart';
import 'package:card_app/pages/main_page/widgets/credit_card_swiper.dart';
import 'package:card_app/res/color.dart';
import 'package:card_app/res/data.dart';
import 'package:card_app/res/strings.dart';
import 'package:card_app/utils/color_randomizer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MainPage extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: BLUE,
        child: Stack(
          children: <Widget>[

            InformationSection(
              informationList: actionsDataList.map<InformationDTO>((map) => InformationDTO.fromJson(map)).toList(),
              padding: EdgeInsets.only(top: 150),
              margin: EdgeInsets.only(top: 150),
            ),
            CreditCardSwiper(
              cardList: creditCardList.map<CreditCardDTO>((map) => CreditCardDTO.fromJson(map)).toList(),
            ),
          ],
        ),
      ),
    );
  }
}




class InformationSection extends StatelessWidget {
  final List<InformationDTO> informationList;
  final EdgeInsets margin;
  final EdgeInsets padding;

  InformationSection({@required this.informationList, this.margin, this.padding});
  @override

  Map<String,List<InformationDTO>> dateListSeparate(List<InformationDTO> list) {
    Map<String,List<InformationDTO>> dateMap = Map<String,List<InformationDTO>>();

    for(InformationDTO informationDTO in list) {
      if(dateMap.containsKey(informationDTO.date)) {
        dateMap[informationDTO.date].add(informationDTO);
      } else {
        dateMap[informationDTO.date] = [informationDTO];
      }
    }
    return dateMap;
  }


  Widget build(BuildContext context) {
    Radius radius = Radius.circular(30);

    Map<String,List<InformationDTO>> data = dateListSeparate(informationList);

    List<String> dates = [];

    data.forEach((key,value) => dates.add(key));
    print('dates = $dates');
    return Container(
      width: double.infinity,
      margin: margin,
      padding: padding,
      decoration: BoxDecoration(
        color: WHITE,
        borderRadius: BorderRadius.only(
          topRight: radius,
          topLeft: radius,
        )
      ),
      child: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: dates.length,
                itemBuilder: (BuildContext context, int index) {
                return InformationDayListBuilder(
                  list: data[dates[index]],
                );
              }
            ),
          ),
        ],
      )
    );
  }
}

class InformationDayListBuilder extends StatelessWidget {
  final List<InformationDTO> list;

  InformationDayListBuilder({this.list});
  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        Text(
          list[0].date
        ),
        ListView.separated(
          separatorBuilder: (BuildContext context, int index) {
            return Container(
              width: double.infinity,
              height: 1.0,
              margin: EdgeInsets.symmetric(horizontal: 5.0),
              color: Colors.grey.withOpacity(0.2),
            );
          },
          itemCount: list.length,
          itemBuilder: (BuildContext context, int index) {
            return InformationWidget(
              informationDTO: list[index],
            );
          }
        ),
      ],
    );
  }
}


class InformationWidget extends StatelessWidget {
  final InformationDTO informationDTO;

  InformationWidget({@required this.informationDTO});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: 40,
            height: 40,
            decoration: BoxDecoration(
              color: colorRandomizer(),
              borderRadius: BorderRadius.all(Radius.circular(10.0))
            ),
            child: Center(
              child: Icon(
                Icons.add,
                color: WHITE,
              ),
            ),
          ),
          Expanded(
            child: Container(
              height: 40.0,
              margin: EdgeInsets.symmetric(horizontal: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    informationDTO.name,
                    //TODO: remove text styles
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                      color: BLACK,
                    ),
                  ),
                  Text(
                    informationDTO.status,
                    style: TextStyle(
                      fontSize: 12.0,
                      fontWeight: FontWeight.w400,
                      color: BLACK.withOpacity(0.5),
                    ),
                  )
                ],
              ),
            ),
          ),
          Align(
            child: Container(
              child: Text(
                informationDTO.type+informationDTO.sum+informationDTO.value,
                style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: 17.0,
                  letterSpacing: -1,
                  color: informationDTO.type == INVOICE ? PURPLE : RED
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

