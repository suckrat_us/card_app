
const List<Map<String,dynamic>> actionsDataList = [
  {
    'date' : '12.03.2020',
    'name' : 'Water bill',
    'status': 'Successfully',
    'sum' : '300.00',
    'type' : '+',
    'value' : "\$"
  },
  {
    'date' : '12.03.2020',
    'name' : 'Water bill',
    'status': 'Successfully',
    'sum' : '300.00',
    'type' : '+',
    'value' : "\$"
  },
  {
    'date' : '12.03.2020',
    'name' : 'Water bill',
    'status': 'Successfully',
    'sum' : '300.00',
    'type' : '+',
    'value' : "\$"
  },
  {
    'date' : '12.03.2020',
    'name' : 'Water bill',
    'status': 'Successfully',
    'sum' : '3210.00',
    'type' : '-',
    'value' : "\$"
  },
  {
    'date' : '12.03.2020',
    'name' : 'Water bill',
    'status': 'Successfully',
    'sum' : '300.00',
    'type' : '+',
    'value' : "\$"
  },

  {
    'date' : '12.03.2020',
    'name' : 'Water bill',
    'status': 'Successfully',
    'sum' : '300.00',
    'type' : '+',
    'value' : "\$"
  },
  {
    'date' : '12.03.2020',
    'name' : 'Water bill',
    'status': 'Successfully',
    'sum' : '3210.00',
    'type' : '-',
    'value' : "\$"
  },
  {
    'date' : '12.03.2020',
    'name' : 'Water bill',
    'status': 'Successfully',
    'sum' : '300.00',
    'type' : '+',
    'value' : "\$"
  },
];

const List<Map<String,dynamic>> creditCardList = [
  {
    'id' : '1',
    'name' : 'Eugene Kan',
    'number': '1234 5678 9012 3456',
    'cash' : '123123.00',
    'type' : 'visa',
    'sub_name' : 'some subname',
    'value' : "\$"
  },
  {
    'id' : '2',
    'name' : 'Eugene Kan',
    'number': '1234 5678 9012 3456',
    'cash' : '123123.00',
    'type' : 'master_card',
    'sub_name' : 'some subname',
    'value' : "\$"
  },
  {
    'id' : '2',
    'name' : 'Eugene Kan',
    'number': '1234 5678 9012 3456',
    'cash' : '123123.00',
    'type' : 'master_card',
    'sub_name' : 'some subname',
    'value' : "\$"
  },

  {
    'id' : '2',
    'name' : 'Eugene Kan',
    'number': '1234 5678 9012 3456',
    'cash' : '123123.00',
    'type' : 'master_card',
    'sub_name' : 'some subname',
    'value' : "\$"
  },
  {
    'id' : '2',
    'name' : 'Eugene Kan',
    'number': '1234 5678 9012 3456',
    'cash' : '123123.00',
    'type' : 'master_card',
    'sub_name' : 'some subname',
    'value' : "\$"
  },
];