import 'package:flutter/material.dart';

const Color WHITE = Colors.white;
const Color BLUE = Colors.blue;
const Color PINK = Colors.pink;
const Color AMBER = Colors.amber;
const Color BLACK = Colors.black;

const Color PURPLE = Color(0xff00008b);
const Color RED = Colors.redAccent;

