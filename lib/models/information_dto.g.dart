// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'information_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InformationDTO _$InformationDTOFromJson(Map<String, dynamic> json) {
  return $checkedNew('InformationDTO', json, () {
    final val = InformationDTO(
      name: $checkedConvert(json, 'name', (v) => v as String),
      status: $checkedConvert(json, 'status', (v) => v as String),
      sum: $checkedConvert(json, 'sum', (v) => v as String),
      type: $checkedConvert(json, 'type', (v) => v as String),
      value: $checkedConvert(json, 'value', (v) => v as String),
    );
    $checkedConvert(json, 'date', (v) => val.date = v as String);
    return val;
  });
}

Map<String, dynamic> _$InformationDTOToJson(InformationDTO instance) =>
    <String, dynamic>{
      'date': instance.date,
      'name': instance.name,
      'status': instance.status,
      'sum': instance.sum,
      'type': instance.type,
      'value': instance.value,
    };
