// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'credit_card_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreditCardDTO _$CreditCardDTOFromJson(Map<String, dynamic> json) {
  return $checkedNew('CreditCardDTO', json, () {
    final val = CreditCardDTO(
      id: $checkedConvert(json, 'id', (v) => v as String),
      name: $checkedConvert(json, 'name', (v) => v as String),
      number: $checkedConvert(json, 'number', (v) => v as String),
      cash: $checkedConvert(json, 'cash', (v) => v as String),
      type: $checkedConvert(json, 'type', (v) => v as String),
      subName: $checkedConvert(json, 'sub_name', (v) => v as String),
      value: $checkedConvert(json, 'value', (v) => v as String),
    );
    return val;
  }, fieldKeyMap: const {'subName': 'sub_name'});
}

Map<String, dynamic> _$CreditCardDTOToJson(CreditCardDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
      'number': instance.number,
      'cash': instance.cash,
      'type': instance.type,
      'sub_name': instance.subName,
      'value': instance.value,
    };
