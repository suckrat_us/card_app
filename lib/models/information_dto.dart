import 'package:json_annotation/json_annotation.dart';

part 'information_dto.g.dart';

@JsonSerializable(
  fieldRename: FieldRename.snake,
  checked: true,
  explicitToJson: true,
)
class InformationDTO {
  @JsonKey(name: "date", required: false, nullable: true)
  String date;
  @JsonKey(name: "name", required: false, nullable: true)
  String name;
  @JsonKey(name: "status", required: false, nullable: true)
  String status;
  @JsonKey(name: "sum", required: false, nullable: true)
  String sum;
  @JsonKey(name: "type", required: false, nullable: true)
  String type;
  @JsonKey(name: "value", required: false, nullable: true)
  String value;

  InformationDTO({
    this.name,
    this.status,
    this.sum,
    this.type,
    this.value,
  });

  factory InformationDTO.fromJson(json) => _$InformationDTOFromJson(json);
  Map<String, dynamic> toJson() => _$InformationDTOToJson(this);
}