import 'package:json_annotation/json_annotation.dart';

part 'credit_card_dto.g.dart';

@JsonSerializable(
  fieldRename: FieldRename.snake,
  checked: true,
  explicitToJson: true,
)
class CreditCardDTO {
  @JsonKey(name: "id", required: false, nullable: true)
  String id;
  @JsonKey(name: "name", required: false, nullable: true)
  String name;
  @JsonKey(name: "number", required: false, nullable: true)
  String number;
  @JsonKey(name: "cash", required: false, nullable: true)
  String cash;
  @JsonKey(name: "type", required: false, nullable: true)
  String type;
  @JsonKey(name: "sub_name", required: false, nullable: true)
  String subName;
  @JsonKey(name: "value", required: false, nullable: true)
  String value;

  CreditCardDTO({
    this.id,
    this.name,
    this.number,
    this.cash,
    this.type,
    this.subName,
    this.value,
  });

  factory CreditCardDTO.fromJson(json) => _$CreditCardDTOFromJson(json);
  Map<String, dynamic> toJson() => _$CreditCardDTOToJson(this);
}